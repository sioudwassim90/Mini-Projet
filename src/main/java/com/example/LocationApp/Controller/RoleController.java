package com.example.LocationApp.Controller;

import com.example.LocationApp.Model.Role;
import com.example.LocationApp.Services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping("/AddRole")
    public Role RegistreNewRole(@RequestBody Role role){ return roleService.AddRole(role);}

}

