package com.example.LocationApp.Controller;

import com.example.LocationApp.Model.Maison;
import com.example.LocationApp.Model.User;
import com.example.LocationApp.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;

@CrossOrigin("http://localhost:4200")

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostConstruct
    public void init(){
        userService.init();
    }

    @PostMapping("/RegistreUser")
    public User RegistreNewUser(@RequestBody User user){
        return userService.RegistreNewUser(user);
    }


    @PostMapping("/AddMaison")
    public Maison addMaison(@RequestBody Maison maison) {
        return userService.AddMaison(maison);
    }
    @GetMapping("/getuser/{username}")
    public Set<Maison> Getbyunmae(@PathVariable String username){
        return userService.GetUserbyuname(username);
    }
    @GetMapping("/All")
    public ResponseEntity<List<Maison>> FindAllMaison(){
        List<Maison> maisons = this.userService.AllMaison();
        return new ResponseEntity(maisons, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMaison(@PathVariable("id") Long id) {
        userService.deleteMaisonn(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
