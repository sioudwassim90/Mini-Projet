package com.example.LocationApp.Dao;

import com.example.LocationApp.Model.contrat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratDao extends JpaRepository<contrat, Long> {
}
