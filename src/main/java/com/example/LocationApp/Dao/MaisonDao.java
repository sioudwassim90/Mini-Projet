package com.example.LocationApp.Dao;

import com.example.LocationApp.Model.Maison;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaisonDao extends JpaRepository<Maison,Long> {
}
