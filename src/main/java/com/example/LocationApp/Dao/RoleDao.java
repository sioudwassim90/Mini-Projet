package com.example.LocationApp.Dao;

import com.example.LocationApp.Model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<Role,String> {
}

