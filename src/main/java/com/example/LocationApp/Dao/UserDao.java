package com.example.LocationApp.Dao;

import com.example.LocationApp.Model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User,String> {
}
