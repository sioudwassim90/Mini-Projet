package com.example.LocationApp.Model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class User {
    @Id
    public String Username;
    public String Nom;
    public String Prenom;
    public int Tel;
    public String Email;
    public String Motpasse;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "user")
    public Set<Maison> maison;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "User_Role", joinColumns =
            {
                    @JoinColumn(name = "User_Name")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "Role_Name")
            }
    )
    public Set<Role> role;

    @OneToMany(mappedBy = "user")
    private List<contrat> contrats;

    public User() {
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }



    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public void setTel(int tel) {
        Tel = tel;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMotpasse() {
        return Motpasse;
    }

    public void setMotpasse(String motpasse) {
        Motpasse = motpasse;
    }

    public Set<Role> getRole() {
        return role;
    }

    public int getTel() {
        return Tel;
    }

    public Set<Maison> getMaison() {
        return maison;
    }

    public void setMaison(Set<Maison> maison) {
        this.maison = maison;
    }

    public void setRole(Set<Role> role) {
        this.role = role;
    }


    public List<contrat> getContrats() {
        return contrats;
    }

    public void setContrats(List<contrat> contrats) {
        this.contrats = contrats;
    }
}

