package com.example.LocationApp.Model;


import javax.persistence.*;
import java.util.Date;

@Entity
public class contrat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idcontrat;

    private Date date_location;

    private Date date_finloc;

    private String duree;

    private String prix_total;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="user_contrats")
    private User user;


    public void Contrat(Long idcontrat, Date date_location, Date date_finloc, String duree, String prix_total) {
        this.idcontrat = idcontrat;
        this.date_location = date_location;
        this.date_finloc = date_finloc;
        this.duree = duree;
        this.prix_total = prix_total;
    }

    public void Contrat() {
    }


    public Long getIdcontrat() {
        return idcontrat;
    }

    public void setIdcontrat(Long idcontrat) {
        this.idcontrat = idcontrat;
    }

    public Date getDate_location() {
        return date_location;
    }

    public void setDate_location(Date date_location) {
        this.date_location = date_location;
    }

    public Date getDate_finloc() {
        return date_finloc;
    }

    public void setDate_finloc(Date date_finloc) {
        this.date_finloc = date_finloc;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getPrix_total() {
        return prix_total;
    }

    public void setPrix_total(String prix_total) {
        this.prix_total = prix_total;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}