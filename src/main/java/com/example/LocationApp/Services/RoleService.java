package com.example.LocationApp.Services;

import com.example.LocationApp.Dao.RoleDao;
import com.example.LocationApp.Model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

  @Autowired
    private RoleDao roleDao;

  public Role AddRole(Role role){
    return roleDao.save(role);
  }
}
