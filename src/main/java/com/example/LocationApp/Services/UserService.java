package com.example.LocationApp.Services;

import com.example.LocationApp.Dao.MaisonDao;
import com.example.LocationApp.Dao.RoleDao;
import com.example.LocationApp.Dao.UserDao;
import com.example.LocationApp.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private MaisonDao maisonDao;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public User RegistreNewUser(User user)
    {
        Role role = roleDao.findById("client").get();
        Set<Role> Roles=new HashSet<>();
        Roles.add(role);
        user.setMotpasse(getEncodedPassword(user.getMotpasse()));
        user.setRole(Roles);
        userDao.save(user);
        return user;
    }
    public void init(){
        Role role=new Role();
        role.setNom("client");
        role.setDescription("role for client");
        roleDao.save(role);
    }


    public Maison AddMaison(Maison maison)
    {

        return this.maisonDao.save(maison);
    }

    public Set<Maison> GetUserbyuname(String username){
        return this.userDao.findById(username).get().getMaison();
    }

    public List<Maison> AllMaison(){
        return this.maisonDao.findAll();
    }

    public void deleteMaisonn(Long id) {
        this.maisonDao.deleteById(id);
    }


    public String getEncodedPassword(String password){
        return passwordEncoder.encode(password);
    }
}
